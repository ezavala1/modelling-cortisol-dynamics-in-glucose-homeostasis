### Summary plot ###

import matplotlib.pyplot as plt
import numpy as np
import Parameters as pr
import Functions as fn
    
#%% Calculations

# I generate vectors for capturing the data from healthy subjects.
Gcent=[]; Gmin = []; Gmax = []; Icent = []; Imin = []; Imax = []; Gogtt = []; Iogtt = []; AGogtt =[]; AIogtt = []

# Then, I generate vectors for capturing data for Cushing's syndrome patients
CGcent=[]; CGmin = []; CGmax = []; CIcent = []; CImin = []; CImax = []; CGogtt = []; CIogtt = []; ACGogtt =[]; ACIogtt = []

for pos in range(len(pr.FTs)):
    Gundi = np.load('Simulation_Gu FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    Iundi = np.load('Simulation_Iu FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    GOGTT = np.load('Simulation_G FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    IOGTT = np.load('Simulation_I FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    CGundi = np.load('Cush_Simulation_Gu FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    CIundi = np.load('Cush_Simulation_Iu FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    CGOGTT = np.load('Cush_Simulation_G FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    CIOGTT = np.load('Cush_Simulation_I FT='+str(pr.FTs[pos])+' '+pr.ChP+'=' +str(pr.Prs[-1])+'.npy')
    
    Gcent.append(np.mean(Gundi))
    Icent.append(np.mean(Iundi))
    Gmax.append(max(Gundi))
    Gmin.append(min(Gundi))
    Imax.append(max(Iundi))
    Imin.append(min(Iundi))
    Gogtt.append(max(GOGTT)-GOGTT[0])
    Iogtt.append(max(IOGTT)-IOGTT[0])
    AGogtt.append(max(GOGTT))
    AIogtt.append(max(IOGTT))
    
    CGcent.append(np.mean(CGundi))
    CIcent.append(np.mean(CIundi))
    CGmax.append(max(CGundi))
    CGmin.append(min(CGundi))
    CImax.append(max(CIundi))
    CImin.append(min(CIundi))
    CGogtt.append(max(CGOGTT)-CGOGTT[0])
    CIogtt.append(max(CIOGTT)-CIOGTT[0])
    ACGogtt.append(max(CGOGTT))
    ACIogtt.append(max(CIOGTT))
    
#%% Plot settings

plt.close()
fig1 = plt.figure(1, figsize=(7.5,8), facecolor='white')
plt.clf()        

plt.rcParams.update({'font.size': 14})

c=1; r=2

axl=list()
axr=list()
for rr in np.arange(r*c):
    axl.append(fig1.add_subplot(r,c,rr+1))
    axr.append(axl[rr].twinx())

glu_color = 'royalblue'    
ins_color = 'indianred'

# Plot basal levels
axl[0].plot(pr.FTs,Gcent,'-',color=glu_color,label='Basal glucose')
axl[0].fill_between(pr.FTs,Gmin,Gmax,color=glu_color,alpha=0.2)
axl[0].plot(pr.FTs,CGcent,'--',color=glu_color,label=r'Basal glucose $\uparrow$CORT')
axl[0].fill_between(pr.FTs,CGmin,CGmax,color=glu_color,alpha=0.2)
axr[0].plot(pr.FTs,Icent,'-',color=ins_color,label='Basal insulin')
axr[0].fill_between(pr.FTs,Imin,Imax,color=ins_color,alpha=0.2)
axr[0].plot(pr.FTs,CIcent,'--',color=ins_color,label=r'Basal insulin $\uparrow$CORT')
axr[0].fill_between(pr.FTs,CImin,CImax,color=ins_color,alpha=0.2)

axl[0].set_ylim(3.5,12.5); axr[0].set_ylim(0.0,1.05)
axl[0].set_ylabel('Glucose (mM)')
axr[0].set_ylabel('Insulin (nM)')
axl[0].legend(frameon=False,loc='upper center',bbox_to_anchor=(0.25, 1.3))
axr[0].legend(frameon=False,loc='upper center',bbox_to_anchor=(0.75, 1.3))
axl[0].set_xlabel('Time (24 hrs)')

# Plot OGTT levels
axl[1].plot(pr.FTs,Gogtt,'-',color=glu_color,label='$\Delta$ Glucose')
axl[1].plot(pr.FTs,CGogtt,'--',color=glu_color,label=r'$\Delta$ Glucose $\uparrow$CORT')
axr[1].plot(pr.FTs,Iogtt,'-',color=ins_color,label='$\Delta$ Insulin')
axr[1].plot(pr.FTs,CIogtt,'--',color=ins_color,label=r'$\Delta$ Insulin $\uparrow$CORT')

axl[1].set_ylim(1.8,5.5); axr[1].set_ylim(0.0,1.4)
axl[1].set_ylabel(r'$\Delta$ Glucose (mM)')
axr[1].set_ylabel(r'$\Delta$ Insulin (nM)')
axl[1].legend(frameon=False,loc='upper center',bbox_to_anchor=(0.25, 1.3))
axr[1].legend(frameon=False,loc='upper center',bbox_to_anchor=(0.75, 1.3))
axl[1].set_xlabel('Time (24 hrs)')

time = 60*np.arange(0,25,6)
for i in range(r*c):
    axl[i].set_xticks(time)
    axl[i].set_xticklabels(fn.vhour(time))
    axl[i].set_xlim(0,1440)

fig1.suptitle(r'Increased sensitivity of $\beta$ cells to glucose ($\downarrow G_\beta$)', fontsize=16, fontweight='bold', y=1)

fig1.tight_layout()
plt.show()
    
#%% Calculate AUC

# Basal levels
AUC_Gcent = np.trapz(Gcent)
AUC_CGcent = np.trapz(CGcent)
AUC_Icent = np.trapz(Icent)
AUC_CIcent = np.trapz(CIcent)

print('\n')
print('AUC_Gbas =', AUC_Gcent, 'mM min')
print('AUC_Ibas =', AUC_Icent, 'mM min')
print('AUC_Gbas_hiC =', AUC_CGcent, 'mM min')
print('AUC_Ibas_hiC =', AUC_CIcent, 'mM min')

# Delta levels
AUC_Gogtt = np.trapz(Gogtt)
AUC_CGogtt = np.trapz(CGogtt)
AUC_Iogtt = np.trapz(Iogtt)
AUC_CIogtt = np.trapz(CIogtt)

print('\n')
print('AUC_Gdel =', AUC_Gogtt, 'nM min')
print('AUC_Idel =', AUC_Iogtt, 'nM min')
print('AUC_Gdel_hiC =', AUC_CGogtt, 'nM min')
print('AUC_Idel_hiC =', AUC_CIogtt, 'nM min')

# Percentage change - Basal
Change_G_hiC = (AUC_CGcent*100/AUC_Gcent)-100
Change_I_hiC = (AUC_CIcent*100/AUC_Icent)-100

print('\n')
print('Change_G_hiC =', Change_G_hiC, '%')
print('Change_I_hiC =', Change_I_hiC, '%')

# Percentage change - Delta
Change_Gdel_hiC = (AUC_CGogtt*100/AUC_Gogtt)-100
Change_Idel_hiC = (AUC_CIogtt*100/AUC_Iogtt)-100

print('\n')
print('Change_Gdel_hiC =', Change_Gdel_hiC, '%')
print('Change_Idel_hiC =', Change_Idel_hiC, '%')

#%% Save fig

#plt.savefig('circadian_var Gbeta.pdf')