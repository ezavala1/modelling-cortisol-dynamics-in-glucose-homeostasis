import numpy as np
import matplotlib.pyplot as plt
import functions as fn

#%% Dex is a parameter used in the functions to change from the function with 
#dexamethasone effect to the one that does not have that effect, I just 
#define this value to determine that I am refering to the data without 
#dexamethasone
fn.pars['hQ']=True
fn.pars['Dex']=0

#%% Run the simulation

OmeVal= np.arange(0.0,1.1,0.2)
G=list(); I=list(); T=list()

y = fn.RungeKutta(fn.pars['ic'], fn.equationALL, parNames=['Q','F'], parValues=[fn.Q, fn.F])

T = y[0]; I = y[1]; G = y[2]

#%% Plot the results

plt.close()
fig1 = plt.figure(1, facecolor='white')
plt.clf()

plt.rcParams.update({'font.size': 14})

ax1 = fig1.add_subplot(211)
ax1.set_ylabel('Glucose (mM)')
ax1.plot(fn.times,G,linewidth=2,label='Model')
ax1.axvline(0,linestyle='--',color='r',alpha=0.3)
ax1.axvspan(0,180,facecolor='r',alpha=0.05)
ax1.set_xlim(-720,fn.pars['endtime'])
ax1.set_ylim(3.5,9)
ax1.set_xticks(np.arange(-720,721,240))
ax1.set_xticklabels(['8 pm','12 am','4 am','8 am','12 pm','4 pm','8 pm'])
ax1.legend(frameon=False,fontsize='small')

ax2 = fig1.add_subplot(212)
ax2.set_ylabel('Insulin (nM)')
ax2.plot(fn.times,I,linewidth=2,label='Model')
ax2.axvline(0,linestyle='--',color='r',alpha=0.3)
ax2.axvspan(0,180,facecolor='r',alpha=0.05)
ax2.set_xlim(-720,fn.pars['endtime'])
ax2.set_ylim(0.07,0.5)
ax2.set_xticks(np.arange(-720,721,240))
ax2.set_xticklabels(['8 pm','12 am','4 am','8 am','12 pm','4 pm','8 pm'])

ax2.set_xlabel('Clock time (hrs)')

fig1.tight_layout()
plt.show()

#%% Save figure

#plt.savefig('24h simulation.pdf')
