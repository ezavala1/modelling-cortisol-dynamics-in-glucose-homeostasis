# In[1]: Import necessary packages and scripts

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from scipy.interpolate import interp1d as inter
import functions as fn

fn.pars['hQ']=True
fn.pars['Dex']=1    ### This is used to change to data from Kautzky-Willer (1996) before dexamethasone administration (0) and after it (1)
# In[4]: Least Squares Optimisation

#This section should be modified in order to change the conditions in wich the system is being optimized
Names=('omegah','omegaf','Dh','Df')          #Define the names of the parameters that are going to be optimized
x0 = np.array([0.1,0.088,0.88,1.23])              # Define initial conditions for the parameters
bnds = ((0.01,0.5),(0.01,0.5),(0.2,2.0),(0.2,2.0))    # Define bounds if necessary


def sqmin(VarIVal, VarNam=Names, p=fn.pars):
    
    # Define the parameters to be optimised 
    nOptim=len(VarNam)
    
    for nn in range (0,nOptim):
        p[VarNam[nn]]=VarIVal[nn]

    # Run simulation and save the glucose and insulin responses
    y = fn.RungeKutta(fn.pars['ic'], fn.equationALL, parNames=['Q','F'], parValues=[fn.Q, fn.F]) 
    I = y[1]; G = y[2]  
    
    Ginter = inter(fn.times, G); Iinter = inter(fn.times, I)
     
    # Least squares minimisation, the experimental data which is used for comparation here is provided in the function file
    return sum(np.sqrt((fn.Glucose[fn.pars['Dex']]-Ginter(fn.GlucTimes[fn.pars['Dex']]))**2 + (fn.Insulin[fn.pars['Dex']]-Iinter(fn.InTimes[fn.pars['Dex']]))**2)) 



# In[]: Optimisation of Parameters


# Method can be changed to Nelder-Mead if bounds are not required

optival = opt.minimize(sqmin, x0,method='TNC', options={'maxiter':1000},bounds=bnds)

 #Print the result of Optimisation
print(optival)

# In[]

y = fn.RungeKutta(fn.pars['ic'], fn.equationALL, parNames=['Q','F'], parValues=[fn.Q, fn.F])
G=y[2]; I=y[1]

# In[]


# Initiate plots
plt.rc('xtick', labelsize=15) ; plt.rc('ytick', labelsize=15) 
plt.rcParams.update({'font.size': 15})
fig=plt.figure(figsize=(15,10)); plt.ioff() 
r=2; c=1; ax=list()
for rr in np.arange(r*c):
    ax.append(fig.add_subplot(r,c,rr+1))

# Glucose plot: model simulation against experimental data
ax[0].plot(fn.GlucTimes[fn.pars['Dex']],fn.Glucose[fn.pars['Dex']],'blue', marker = 'o',label='Experimental values')
ax[0].plot(fn.times,G,'k',label='Model prediction')
ax[0].set_ylabel('Glucose (mM)')

# Insulin plot: model simulation against experimental data
ax[1].plot(fn.InTimes[fn.pars['Dex']],fn.Insulin[fn.pars['Dex']],'blue', marker = 'o',label='Experimental values')
ax[1].plot(fn.times,I,'k',label='Model prediction')
ax[1].set_ylabel('Insulin (nM)')

ax[r-1].set_xlabel('Time (min)')        # label time axis
for rr in np.arange(r*c):
    ax[rr].legend(loc='right')    # Plot legends
    ax[rr].set_xlim(0,fn.pars['endtime'])    # X axis limits
    
#This creates a note for the new values of the variables that were optimised
Text=''
for nn in range (0,len(Names)):
    fn.pars[Names[nn]]=optival.x[nn]
    TVal=str(Names[nn]) + ' = ' + str(optival.x[nn]) + ', '
    Text=Text+TVal
plt.text(0.0,-0.05,Text)

#plt.savefig('Optimisation.pdf')

