import numpy as np
# Set up a dictionary of parameter names and values

pars = {} # parameter array

# Initial conditions for the system

diction= {'ic':np.array([0.70753726, 0.17109632, 4.12139851])}; pars.update(diction)

# Parameters for Proportion of Membrane Transporters in Plasma Membrane
diction = {'vI':0.023,'vQ':0.014,'Im':0.081,'Qm':0.28,'u':0.01,'d':0.0049}; pars.update(diction)

# Parameters for the feeding function

diction = {'begin':200.0, 'divisions':40.0,'FT':0.0}; pars.update(diction)

# Parameters for the Insulin Equation
diction = {'sigma':0.48,'rI':0.47,'epsilon':0.02}; pars.update(diction);  
 
# Parameters for the Glucose Equation
diction = {'a':17.97,'cL':0.66,'cM':0.34,'rG':0.2,'Ge':5.0,'v':18.69,'Gbeta':6.48,'alpha':2.72,'GL':1.4,'GM':4.0,'F':0.0}; pars.update(diction) 

# Parameters for Cortisol Function
diction = {'AG':0.32,'Am':0.16,'del':780.0,'Tc':1440.0,'Tu':75.0,'B':0.04,'Q':0.0}; pars.update(diction);

#  Parameters for Feeding and Insulin Inhibition Functions
diction = {'omegah':1.0, 'Dh':0.0,'omegaf':1.0, 'Df':0.0, 'kQ':0.4, 'n':4.48, 'A':3.7, 'tau':30.0,'starttime':-500.0, 'endtime':2880.0, 'step':1.0/100.0}; pars.update(diction);

# Parameters of state
diction = {'hQ':True,'Dex':0}; pars.update(diction)


# I save the original values of the parameters, to use them freely.
osig= pars['sigma']
ov= pars['v']
oim = pars['Im']

# Ranges for the parameter change

sigmin = int(0*osig)
sigmax = 200*osig+1
vmin = int(100*ov)
vmax = int(125*ov+1)
immin = int(100*oim)
immax = int (4000*oim+1)

#Here I capture the data, as reported in DeFronzo, for comparision proposes.
#DFx = np.array([73.80460683081812, 89.8173153296267, 114.02700555996822, 139.95234312946786, 158.25258141382048, 177.69658459094518, 220.58776806989673])
DFx = np.array([73.80460683081812, 89.8173153296267, 114.02700555996822, 139.95234312946786, 158.25258141382048, 177.69658459094518])
#DFy_I = np.array([48.14814814814815, 60.56056056056057, 100.20020020020021, 64.36436436436436, 42.54254254254255, 29.129129129129133, 20.920920920920917])
DFy_I = np.array([48.14814814814815, 60.56056056056057, 100.20020020020021, 64.36436436436436, 42.54254254254255, 29.129129129129133])
DFy_G = np.array([114.03940887, 116.99507389, 160.34482759, 240.64039409, 292.85714286, 361.57635468])
DFy_I_Err = np.array([8.80635893, 13.34374082, 12.89370719, 21.92735818, 11.56246466, 6.17170225])
DFy_G_Err = np.array([25.12315271, 19.21182266, 24.13793103, 33.99014778, 40.88669951, 46.30541872])

# Now I do the conversion to appropiate units
DFx = DFx*11.1/200
DFy_I = DFy_I*0.6945/100
DFy_I_Err = DFy_I_Err*0.6945/(100*2)
DFy_G = DFy_G*11.1/200
DFy_G_Err = DFy_G_Err*11.1/(200*2)
