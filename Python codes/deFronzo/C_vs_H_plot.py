import matplotlib.pyplot as plt
import numpy as np
import Parameters as pr
from scipy.ndimage.filters import gaussian_filter1d

#%% Load data points

X=np.load('Values for X.npy')
Y=np.load('Values for Y.npy')
CushV = np.load('CValues.npy')
OGTTG = np.load('Values for OGTTG.npy')

#%% Gaussian filtering
sigma=6
Xs = gaussian_filter1d(X, sigma=sigma)
Ys = gaussian_filter1d(Y, sigma=sigma)
OGTTGs = gaussian_filter1d(OGTTG, sigma=sigma)
CushVs = gaussian_filter1d(CushV, sigma=sigma)

#%% Plot figure
#fig1 = plt.figure(1, facecolor='white', figsize=(6.4,9.6))
fig1 = plt.figure(1, facecolor='white', figsize=(12.8,4.8))
plt.clf()

plt.rcParams.update({'font.size': 14})
ms = 7

ax=list()#; axt = list()
r= 2; c=1
for n in range(r*c):
#    ax.append(fig1.add_subplot(r,c,n+1))
    ax.append(fig1.add_subplot(c,r,n+1))

ax[0].plot(Xs,Ys,'.g',alpha=0.3,markersize=ms,label='Model normal CORT')
ax[0].plot(CushVs[0],CushVs[1],'.r',alpha=0.3,markersize=ms,label='Model Hypercortisolism')
ax[0].errorbar(pr.DFx,pr.DFy_I,yerr=pr.DFy_I_Err,fmt='ko',label='Data')
ax[0].set_xlim(3.8,11.5)
ax[0].set_ylim(0.15,0.8)
ax[0].set_xlabel('Fasting Glucose (mM)')
ax[0].set_ylabel('Insulin post OGTT (nM)')
#ax[0].legend(frameon=False,fontsize='small',loc='upper right')

ax[1].plot(Xs,OGTTGs,'g.',label='Model normal CORT')
ax[1].plot(CushVs[0],CushVs[2],'r.',label='Model Hypercortisolism')
ax[1].errorbar(pr.DFx,pr.DFy_G,yerr=pr.DFy_G_Err,fmt='ko',label='Data')
ax[1].set_xlim(3.8,11.5)
ax[1].set_xlabel('Fasting Glucose (mM)')
ax[1].set_ylabel('Glucose post OGTT (mM)')
ax[1].legend(frameon=False,fontsize='small',loc='upper left')

fig1.tight_layout()
plt.show()

#%% Save figure

#plt.savefig('deFronzo_normal_and_HiCORT.pdf')
