import numpy as np
import matplotlib.pyplot as plt
import sympy as sn
import functions as fn
import parameters as pr

#%% Data

name=['Control','B_0_15','B_0_4','No_Ultradian','No_Circadian']

AmpG=([]); AmpI=([]); AbsG=([]); AbsI=([])

for i in range(len(name)):
    AmpG.append(np.load('Simulation_AmpG_'+str(name[i])+'.npy'))
    AmpI.append(np.load('Simulation_AmpI_'+str(name[i])+'.npy'))
    AbsG.append(np.load('Simulation_AbsG_'+str(name[i])+'.npy'))
    AbsI.append(np.load('Simulation_AbsI_'+str(name[i])+'.npy'))

#%% Calculations

MAmpG = np.max(AmpG[0]); MAmpI = np.max(AmpI[0]); MAbsG = np.max(AbsG[0]); MAbsI = np.max(AbsI[0]);
mAmpG = np.min(AmpG[0]); mAmpI = np.min(AmpI[0]); mAbsG = np.min(AbsG[0]); mAbsI = np.min(AbsI[0]);

for i in range(len(name)):
    if np.max(AmpG[i]) > MAmpG:
        MAmpG = np.max(AmpG[i])
    if np.max(AmpI[i]) > MAmpI:
        MAmpI = np.max(AmpI[i])
    if np.max(AbsG[i]) > MAbsG:
        MAbsG =np.max(AbsG[i])
    if np.max(AbsI[i]) > MAbsI:
        MAbsI = np.max(AbsI[i])

    if np.min(AmpG[i]) < mAmpG:
        mAmpG = np.min(AmpG[i])
    if np.min(AmpI[i]) < mAmpI:
        mAmpI = np.min(AmpI[i])
    if np.min(AbsG[i]) < mAbsG:
        mAbsG =np.min(AbsG[i])
    if np.min(AbsI[i]) < mAbsI:
        mAbsI = np.min(AbsI[i])

#%% Plotting settings

pi = sn.symbols('π')

FTs = np.arange(0,pr.pars['Tu'] + 1/pr.pars['divisions'] ,pr.pars['Tu']/pr.pars['divisions'])
HD = np.arange(0,fn.pars['Tc'],fn.pars['Tu'])

nHD = fn.vhour(HD)
redlab = np.arange(0,1.0,0.25)

nPer = []
nPerLab = []
for i in range(len(redlab)):
    nPer.append(int(len(FTs)*redlab[i]))
    nPerLab.append(redlab[i]*pi)

#%% Plot

plt.close('all')

for i in range(0,len(AmpG)):
    pos=i

    fig=plt.figure(figsize=(14,10),facecolor='white'); plt.ioff()

    # Multipanel fig settings
    r=1; c=2; ax=list()
    for rr in np.arange(r*c):
        ax.append(fig.add_subplot(r,c,rr+1))

    # Heatmaps
    imG = ax[0].imshow(AmpG[pos],cmap='Blues'); imI = ax[1].imshow(AmpI[pos],cmap='OrRd')
#    imGabs = ax[2].imshow(AbsG[pos]); imIabs = ax[3].imshow(AbsI[pos])

    #This set the minimal and maximal values for the axes in all the simulations
    imG.set_clim(mAmpG,MAmpG); imI.set_clim(mAmpI,MAmpI)
#    imGabs.set_clim(mAbsG,MAbsG); imIabs.set_clim(mAbsI,MAbsI)

    for i in range(0,r*c):
        ax[i].set_xticks(np.arange(len(HD)))
        #ax[i].set_yticks(np.arange(len(FTs)))
        ax[i].set_yticks(nPer)

        ax[i].set_xticklabels(nHD,fontsize=12)
        #ax[i].set_yticklabels(np.around(FTs/pr.pars['Tu'],decimals=2)*pi)
        ax[i].set_yticklabels(nPerLab,fontsize=14)
        ax[i].set_xlabel('Time (24 hrs)',fontsize=20)
        ax[i].set_ylabel('Ultradian phase (rads)',fontsize=20)
        #ax[i].xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        plt.setp(ax[i].get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")

    cAmpG=plt.colorbar(imG,ax=ax[0])
    cAmpI=plt.colorbar(imI,ax=ax[1])
#    cAbsG=plt.colorbar(imGabs,ax=ax[2],label='Amplitude (mM)')
#    cAbsI=plt.colorbar(imIabs,ax=ax[3],label='Amplitude (nM)')

    cAmpG.ax.tick_params(labelsize=14)
    cAmpI.ax.tick_params(labelsize=14)
    cAmpG.set_label(label='Amplitude (mM)',fontsize=20)
    cAmpI.set_label(label='Amplitude (mM)',fontsize=20)

    ax[0].set_title('$\Delta$ Glucose post OGTT',fontsize=20)
    ax[1].set_title('$\Delta$ Insulin post OGTT',fontsize=20)
#    ax[2].set_title('Glucose post OGTT')
#    ax[3].set_title('Insulin post OGTT')

    fig.tight_layout()

    plt.show()

#%% Save figures

#    plt.savefig('Heatmap_'+name[pos] +'.pdf')
