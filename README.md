# Dynamic modulation of glucose utilisation by glucocorticoid rhythms in health and disease

Codes associated to the paper https://www.biorxiv.org/content/10.1101/2020.02.27.968354v1.abstract

*Zavala E, Gil-Gómez CA, Wedgwood KCA, Burgess R, Tsaneva-Atanasova K, Herrera-Valdez MA. 2020 Dynamic modulation of glucose utilisation by glucocorticoid rhythms in health and disease*

**Python 3.6 code to run model simulations**

Pyhton codes contains:

*  24h simulation
*  circadian var
*  CORT dynamics
*  deFronzo
*  heatmaps
*  OGTT
*  OGTT w/antagonist
*  OGTT w/Dex
*  OGTT w/Dex comparison
