import numpy as np
import Parameters as pr
import Functions as fn
import matplotlib.pyplot as plt
import time
import multiprocessing as mp

# In[]
Ims=np.load('Values for Im.npy')
sigmas=np.load('Values for sigma.npy')
vs=np.load('Values for v.npy')
Xs=np.load('Values for X.npy')
Ys=np.load('Values for Y.npy')

# In[]
pr.pars['B']=0.4
def Csim(pos):
#for i in range(len(Xs)):
    pr.pars['Im']=Ims[pos]
    pr.pars['sigma']=sigmas[pos]
    pr.pars['v']=vs[pos]
    
    y = fn.RungeKutta(pr.pars['ic'], fn.equationALL, parNames=['Q','F'], parValues=[fn.Q, fn.F])
    I = y[1]; G = y[2]
    
    sft=fn.nxmaxloc(fn.Q,pr.pars['FT'])
    ini=fn.index(fn.times,sft-pr.pars['Tu'])
    end=fn.index(fn.times,sft+pr.pars['Tu'])
    
    Gint=G[ini:end]
    Iint=I[ini:end]
    
    MeanG = np.mean(Gint)
    Idif = np.mean(Iint)
    
    #CXs.append(MeanG)
    #CYs.append(Idif)
    return (MeanG,Idif)
    
if __name__=='__main__':
    start=time.time()
    numbers=range(len(Xs))
    #numbers = range(2)
    pool=mp.Pool(4)
    result = pool.map(Csim, numbers)
    end=time.time()
    print(end-start)

# In[]
result=np.array(result)    
CushV=result.transpose()   
# In[]
#np.save('Cushing X',CXs)
#np.save('Cushing Y',CYs)
#np.save('CValues',CushV)

# In[]

Av= plt.figure(figsize=(10,5))
ax=list()#; axt = list()
r= 1; c=1
for n in range(r*c):
    ax.append(Av.add_subplot(r,c,n+1))
    #axt.append(ax[n].twinx())
    

ax[0].plot(Xs,Ys,'g.',label='B=0.04')
ax[0].plot(CushV[0],CushV[1],'r.',label='B=0.4')
ax[0].plot(pr.DFx,pr.DFy,'kx',label='DeFronzo, 1988 data',mew=2.0)
plt.legend()

plt.show()

#plt.savefig('Cushing comparision.pdf')