import functions as fn
import parameters as pr
import numpy as np
import matplotlib.pyplot as plt
import sympy as sn
import multiprocessing as mp
import os

#This part is to stablish the conditions in which the simulation is done, hQ incorporates the hQ part of the equation (or not) and Dex is for which experimental values it uses (Kautzky-Willer)
fn.pars['hQ']=True
fn.pars['Dex']=0
# In[]
FTs = np.arange(0,pr.pars['Tu'] + 1/pr.pars['divisions'] ,pr.pars['Tu']/pr.pars['divisions'])
HD = np.arange(0,fn.pars['Tc'],fn.pars['Tu'])
output=mp.Queue()

AmpG = np.zeros((len(FTs),len(HD)))
AmpI = np.zeros((len(FTs),len(HD)))
AbsG = np.zeros((len(FTs),len(HD)))
AbsI = np.zeros((len(FTs),len(HD)))

# In[]

def mat(number):

    fn.pars['FT']=FTs[number%len(FTs)] + HD[int(number/len(FTs))]
    fn.pars['endtime'] = fn.pars['FT'] + 2*fn.pars['tau']
    fn.times = np.arange(fn.pars['starttime'],fn.pars['endtime'],fn.pars['step'])
    fn.F = fn.feeding(fn.times,fn.pars['FT'])
    
    y = fn.RungeKutta(fn.pars['ic'], fn.equationALL, parNames=['Q','F'], parValues=[fn.Q, fn.F])
    I = y[1]; G = y[2]
    
    Gint = G[fn.index(fn.times,fn.pars['FT']):fn.index(fn.times,fn.pars['FT']) + int((fn.pars['Tu'] + fn.pars['tau'])/fn.pars['step'])]
    Iint = I[fn.index(fn.times,fn.pars['FT']):fn.index(fn.times,fn.pars['FT']) + int((fn.pars['Tu'] + 2*fn.pars['tau'])/fn.pars['step'])]
      
    #This generates an arange that is going to collect the data for this simulation and take it out of the subprocess
    rev=np.arange(4.0)
    #Here is the point in which the actual data collecting takes place
    #AmpG
    rev[0] = max(Gint) - G[fn.index(fn.times,fn.pars['FT'])]
    #AmpI
    rev[1] = max(Iint) - I[fn.index(fn.times,fn.pars['FT'])]
    #AbsG
    rev[2] = max(Gint)
    #AbsI
    rev[3] = max(Iint)
    
    #print('Missing simulations to run: '+ str(len(FTs)*len(HD)-number-1))
    #Here I take the arange rev out of this subprocess, the capture in the matrix is done outside
    output.put(rev)

# In[]

if __name__=='__main__':
    processes = []
    numbers=range(0,len(HD)*len(FTs))
   
    for number in numbers:
        process = mp.Process(target=mat,args=(number,))
        processes.append(process)
        process.start()
        
        #The arange vector is here taken out of the subprocess and put into a new name "results"
        results = output.get()
        
        #Here I capture the data present in the arange results and put each data in the corresponding matrix, the coordenates are given by the number count, so it should not have problem with bizarre orderings.
        AmpG[number%len(FTs),int(number/len(FTs))] = results[0]
        AmpI[number%len(FTs),int(number/len(FTs))] = results[1]
        AbsG[number%len(FTs),int(number/len(FTs))] = results[2]
        AbsI[number%len(FTs),int(number/len(FTs))] = results[3]
        
        process.join()
        
# In[]: The part dedicated to do the actual image

pi = sn.symbols('π')
fig=plt.figure(figsize=(15,15)); plt.ioff()



r=2; c=2; ax=list()
for rr in np.arange(r*c):
    ax.append(fig.add_subplot(r,c,rr+1))
imG = ax[0].imshow(AmpG); imI = ax[1].imshow(AmpI)   
imGabs = ax[2].imshow(AbsG); imIabs = ax[3].imshow(AbsI)
    

for i in range(0,r*c):
    ax[i].set_xticks(np.arange(len(HD)))
    ax[i].set_yticks(np.arange(len(FTs)))
    
    ax[i].set_xticklabels(HD)
    ax[i].set_yticklabels(np.around(FTs/fn.pars['Tu'],decimals=2)*pi)
    ax[i].set_xlabel('Time (minutes, starting at 8 am)')  
    #ax[i].xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    plt.setp(ax[i].get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    

plt.colorbar(imG,ax=ax[0],label='Amplitude (mM)')
plt.colorbar(imI,ax=ax[1],label=r'Amplitude (nM)')
plt.colorbar(imGabs,ax=ax[2],label='Amplitude (mM)')
plt.colorbar(imIabs,ax=ax[3],label=r'Amplitude ($\mu$M)')


for i in range(0,r):
    ax[2*i].set_ylabel('Phase of ultradian cycle (radians)')  
ax[0].set_title('Glucose pulse amplitude after OGTT')
ax[1].set_title('Insulin pulse amplitude after OGTT')
ax[2].set_title('Absolute gluose amplitude after OGTT')
ax[3].set_title('Absolute insulin amplitude after OGTT')

fig.tight_layout()
    
#plt.savefig('Simulation.pdf')

# In[]

np.save('Simulation_AmpG',AmpG)
np.save('Simulation_AmpI',AmpI)
np.save('Simulation_AbsG',AbsG)
np.save('Simulation_AbsI',AbsI)

# In[]

duration = 1  # second
freq = 440  # Hz
os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))