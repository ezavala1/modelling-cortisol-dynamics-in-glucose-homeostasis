import numpy as np
import matplotlib.pyplot as plt
import functions as fn

#%% Dex is a parameter used in the functions to change from the function with 
#dexamethasone effect to the one that does not have that effect, I just 
#define this value to determine that I am refering to the data without 
#dexamethasone

fn.pars['hQ']=True
#fn.pars['Dex']=0
fn.pars['Dex']=1

#%% Run the simulation

OmeVal= np.arange(0.0,1.1,0.2)
G=list(); I=list(); T=list()

y = fn.RungeKutta(fn.pars['ic'], fn.equationALL, parNames=['Q','F'], parValues=[fn.Q, fn.F])

T = y[0]; I = y[1]; G = y[2]

#%% Plot the results

#plt.close()
fig1 = plt.figure(1, facecolor='white')
#plt.clf()

plt.rcParams.update({'font.size': 14})

ax1 = fig1.add_subplot(211)
ax1.set_ylabel('Glucose (mM)')
ax1.errorbar(fn.GlucTimes[fn.pars['Dex']],fn.Glucose[fn.pars['Dex']],yerr=fn.GlucoseErr[fn.pars['Dex']],fmt='-ko',lw=1.5,label='Data Dex')
ax1.plot(fn.times,G,linewidth=2,label='Model Dex')
#ax1.plot(fn.times,G,linewidth=2,label='Model')
ax1.set_xlim(0,fn.pars['endtime'])
ax1.set_ylim(4,10)
ax1.set_xticks(np.arange(0,200,30))
ax1.legend(frameon=False,fontsize='small')

ax2 = fig1.add_subplot(212)
ax2.set_ylabel('Insulin (nM)')
ax2.errorbar(fn.InTimes[fn.pars['Dex']],fn.Insulin[fn.pars['Dex']],yerr=fn.InsulinErr[fn.pars['Dex']],fmt='-ko',lw=1.5,label='Data Dex')
ax2.plot(fn.times,I,linewidth=2,label='Model Dex')
#ax2.plot(fn.times,I,linewidth=2,label='Model')
ax2.set_xlim(0,fn.pars['endtime'])
ax2.set_ylim(0,0.83)
ax2.set_xticks(np.arange(0,200,30))
ax2.set_yticks(np.arange(0,0.9,0.2))
ax2.set_xlabel('Time (min)')

fig1.tight_layout()
plt.show()

#%% Save figure

#plt.savefig('OGTT_Dex_comparison.pdf')
