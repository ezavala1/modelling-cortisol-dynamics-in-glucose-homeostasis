import numpy as np
import matplotlib.pyplot as plt
import sympy as sn
import functions as fn
import parameters as pr

# In[]
# Numerical values for each simulation, change the pos value depending on which 
# is desired to graph, it can also be found in the name list
#######
# 0- Control (normal conditions)
# 1- AG=0.7
# 2- B=0.15

pos=2
color='gist_ncar'

name=['Control','AG=0.7','B=0.15','B=0.4']

AmpG=([np.load('Simulation_AmpG_Control.npy'),np.load('Simulation_AmpG_AG_0_7.npy'),np.load('Simulation_AmpG_B_0_15.npy'),np.load('Simulation_AmpG_B_0_4.npy')])
AmpI=([np.load('Simulation_AmpI_Control.npy'),np.load('Simulation_AmpI_AG_0_7.npy'),np.load('Simulation_AmpI_B_0_15.npy'),np.load('Simulation_AmpI_B_0_4.npy')])
AbsG=([np.load('Simulation_AbsG_Control.npy'),np.load('Simulation_AbsG_AG_0_7.npy'),np.load('Simulation_AbsG_B_0_15.npy'),np.load('Simulation_AbsG_B_0_4.npy')])
AbsI=([np.load('Simulation_AbsI_Control.npy'),np.load('Simulation_AbsI_AG_0_7.npy'),np.load('Simulation_AbsI_B_0_15.npy'),np.load('Simulation_AbsI_B_0_4.npy')])



# In[]

pi = sn.symbols('π')
fig=plt.figure(figsize=(15,15)); plt.ioff()
FTs = np.arange(0,pr.pars['Tu'] + 1/pr.pars['divisions'] ,pr.pars['Tu']/pr.pars['divisions'])
HD = np.arange(0,fn.pars['Tc'],fn.pars['Tu'])

r=2; c=2; ax=list()
for rr in np.arange(r*c):
    ax.append(fig.add_subplot(r,c,rr+1))
imG = ax[0].imshow(AmpG[pos]); imI = ax[1].imshow(AmpI[pos])   
imGabs = ax[2].imshow(AbsG[pos]); imIabs = ax[3].imshow(AbsI[pos])

#This set the minimal and maximal values for the axes in all the simulations
imG.set_clim(np.min(AmpG),np.max(AmpG)); imI.set_clim(np.min(AmpI),np.max(AmpI))
imGabs.set_clim(np.min(AbsG),np.max(AbsG)); imIabs.set_clim(np.min(AbsI),np.max(AbsI))

imG.set_cmap(color); imI.set_cmap(color); imGabs.set_cmap(color); imIabs.set_cmap(color)

for i in range(0,r*c):
    ax[i].set_xticks(np.arange(len(HD)))
    ax[i].set_yticks(np.arange(len(FTs)))
    
    ax[i].set_xticklabels(HD)
    ax[i].set_yticklabels(np.around(FTs/pr.pars['Tu'],decimals=2)*pi)
    ax[i].set_xlabel('Time (minutes, starting at 8 am)')  
    #ax[i].xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    plt.setp(ax[i].get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    

#cAmpG=plt.colorbar(imG,ax=ax[0],label='Amplitude (mM)')
cAmpG=plt.colorbar(imG,ax=ax[0],label='Amplitude (mM)')
cAmpI=plt.colorbar(imI,ax=ax[1],label=r'Amplitude ($\mu$M)')
#cAbsG=plt.colorbar(imGabs,ax=ax[2],label='Amplitude (mM)')
cAbsG=plt.colorbar(imGabs,ax=ax[2],label='Amplitude (mM)')
cAbsI=plt.colorbar(imIabs,ax=ax[3],label=r'Amplitude ($\mu$M)')


for i in range(0,r):
    ax[2*i].set_ylabel('Phase of ultradian cycle (radians)')  
ax[0].set_title('Glucose pulse amplitude after OGTT')
ax[1].set_title('Insulin pulse amplitude after OGTT')
ax[2].set_title('Absolute gluose amplitude after OGTT')
ax[3].set_title('Absolute insulin amplitude after OGTT')

fig.tight_layout()
plt.show()

# In[]
#plt.savefig('Simulation '+ name[pos] +'.pdf')