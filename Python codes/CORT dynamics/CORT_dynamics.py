#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 22 14:47:25 2019

@author: ederzavala
"""

import numpy as np
import matplotlib.pyplot as plt

#%% Define CORT dynamics

times = np.arange(0,1440)

pars = {'AG':0.32,
        'Am':0.16,
        'Tc':1400.0,
        'Tu':75.0,
        'B':0.04};

def CORT(t,p):
    
        circa = (np.sin(np.pi*t/p['Tc']))**2   # circadian
        ultra = (np.sin(np.pi*t/p['Tu']))**2   # ultradian
    
        return (p['AG']*circa*ultra + p['Am']*circa + p['B'])

#%% Plot CORT dynamics

fig1 = plt.figure(1, facecolor='white')
plt.clf()

plt.rcParams.update({'font.size': 14})

ax = fig1.add_subplot(111)

ax.plot(times,CORT(times,pars),'-k',linewidth=2,label='CORT normal')

pars['B'] = 0.4
ax.plot(times,CORT(times,pars),'-r',linewidth=2,label='CORT hyp')

pars['B'] = 0.04
pars['AG'] = 0.7
ax.plot(times,CORT(times,pars),'--b',linewidth=2,label='CORT antag')

ax.set_xlim(0,1440)
ax.set_ylim(0,1.05)

ax.set_xticks(np.arange(0,1400,240))
ax.set_xticklabels(['','12 am','4 am','8 am','12 pm','4 pm','8 pm'])
ax.set_ylabel('Cortisol ($\mu$M)')
ax.set_xlabel('Time (hr)')
ax.legend(frameon=False,loc='upper center',bbox_to_anchor=(0.5, 1),ncol=3,fontsize='small')

fig1.tight_layout()
plt.show()

#%% Save figure

#plt.savefig('CORT_dynamics.pdf')
