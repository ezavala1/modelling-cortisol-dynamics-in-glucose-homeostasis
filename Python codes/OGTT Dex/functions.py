#%% Import necessary packages and scripts

import numpy as np
from parameters import pars

#%%  Hill function

def Hill(x,xm,h=2):
   x = x**h
   return x/(x + xm**h)

#%% Define time dependent functions Q(t) and F(t) 

times = np.arange(pars['starttime'],pars['endtime'],pars['step'])  # time array

#%% Experimental data from Kautzky-Willer et al. [1996]

    # Before dexamethasone administration data
glucoseBef = np.array([4.65771812, 5.7852349, 7.46308725, 8.04026846, 7.20805369, 5.87919463, 4.88590604, 4.51006711, 4.60402685])
gluextimesBef = np.array([1.50375940e-01, 9.47368421e+00, 1.90977444e+01, 2.87218045e+01, 5.87969925e+01, 8.94736842e+01, 1.19548872e+02, 1.49624060e+02, 1.79398496e+02])
insulinBef = np.array([0.05024155, 0.14814815, 0.27826087, 0.34782609, 0.42769726, 0.27954911, 0.20483092, 0.13913043, 0.09146538])
inextimesBef = np.array([0.43654002, 10.33144705, 20.22635408, 30.12126112, 60.09700889, 90.36378335, 120.33953112, 150.02425222, 180.58205335])

    # After dexamethasone administration data
glucoseAft = np.array([4.653061224489796, 5.861224489795918, 7.559183673469387, 9.012244897959183, 8.66938775510204, 8.228571428571428, 7.053061224489795, 6.20408163265306, 5.4367346938775505])
gluextimesAft = np.array([0, 9.135888501742144, 18.803384768541555, 28.448481831757086,58.782976605276254, 89.10851169736188, 119.00099552015928, 149.65505226480835, 179.95072175211547])
insulinAft = np.array([0.1535891779194648, 0.38372324177555817, 0.5450750321219908, 0.6952265509466895, 0.6208102014594292, 0.5367936192392544, 0.3679943756212274, 0.19119493805910492, 0.20800019394409597])
inextimesAft = np.array([0, 9.470775049092104,18.77863705786806, 28.096681131663807, 58.70978690392493,89.33162016048874, 119.30325583650512,149.28216441610704,179.08506872893886])

Glucose=(glucoseBef,glucoseAft)
GlucTimes=(gluextimesBef,gluextimesAft)
Insulin=(insulinBef,insulinAft)
InTimes=(inextimesBef,inextimesAft)

#%% Auxiliary functions

# function accounting for cortisol oscillations 
def glucocorticoid(t,p=pars):
    
        circa = (np.sin(np.pi*(t+p['del'])/p['Tc']))**2   # circadian
        ultra = (np.sin(np.pi*(t+p['del'])/p['Tu']))**2   # ultradian
    
        return (p['AG']*circa*ultra + p['Am']*circa + p['B'])
       
Q = glucocorticoid(times) 

# function accounting for feeding inputs
def feeding(t,p=pars):
    sign = np.int32(t>0)
    return sign*p['A']*(t/p['tau'])*np.exp(1-t/p['tau'])   
    
F = feeding(times)

#%% Set up the model
   
# Equation for glucose
def equationG(G,T,p=pars):
 
    # hill function values
    fL = Hill(G,p['GL'],1)        
    fM = Hill(G,p['GM'],1)
    fe = 1-Hill(G,p['Ge'],1)
    
    Gi = p['F'] + p['v']*fe              # feeding, gluconeogenesis, glycogenolysis  
    Uc = p['cL']*fL + p['cM']*fM    # glucose uptake by body cells
    
    dG = Gi - p['a']*Uc*T - p['rG']*G # output
    
    return dG


# Equation for insulin
def equationI(I,G,p=pars): 
    
    Sbeta = Hill(G,p['Gbeta'],p['alpha']) # glucose sensing in beta cells
    if pars['hQ']==True:
        dI = p['epsilon'] + p['sigma']*Sbeta*(1-Hill(p['omegah']*p['Q'],pars['kQ'],pars['n']) + p['Dh']) - p['rI']*I  # output
    else:
        dI = p['epsilon'] + p['sigma']*Sbeta - p['rI']*I  # output
   
    return dI


# Equation for fraction of GLUT transporters
def equationT(T,I,p=pars):

    # hill function values
    fI = Hill(I,p['Im'],2)
    if pars['hQ']==True:
        fQ = Hill(p['omegaf']*p['Q'],p['Qm'],2)+p['Df']
    else:
        fQ = Hill(p['Q'],p['Qm'],2)
    
    dT=(p['u'] + p['vI']*fI)*(1-T) - (p['d']+ p['vQ']*fQ)*T  # output
    
    return dT


# The entire model
def equationALL(U,p=pars):
    T,I,G = U                    # assign values to T,I and G
    dT = equationT(T,I)
    dI = equationI(I,G)
    dG = equationG(G,T)
    
    return np.array([dT,dI,dG])

#%% Method for ODE integration
    
def RungeKutta(ic, f, p=pars, parNames=[], parValues=[]):
        
    n=int((p['endtime']-p['starttime'])/p['step'])
    U = np.zeros((n,np.prod(np.shape(ic))),"float64")
    U[0] = ic
    nForcing=len(parNames)
    if nForcing>0:
        for i in range (0,n-1):
            for nn in range(0,nForcing):
                p[parNames[nn]]=parValues[nn][i]
            k1=f(U[i],p)*p['step']/2.0
            k2=f(U[i]+k1,p)*p['step']
            U[i+1]=U[i]+ k2
    else:
        for i in range (0,n-1):
            k1=f(U[i],p)*p['step']/2.0
            k2=f(U[i]+k1,p)*p['step']
            U[i+1]=U[i]+ k2 
            
    return U.transpose()
