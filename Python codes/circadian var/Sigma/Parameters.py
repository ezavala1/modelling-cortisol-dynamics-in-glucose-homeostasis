import numpy as np
# Set up a dictionary of parameter names and values

pars = {} # parameter array

# Initial conditions for the system

diction= {'ic':np.array([0.70753726, 0.17109632, 4.12139851])}; pars.update(diction)
#diction= {'ic':np.array([0.70753726, 0.0, 4.12139851])}; pars.update(diction)

# Parameters for Proportion of Membrane Transporters in Plasma Membrane
diction = {'vI':0.023,'vQ':0.014,'Im':0.081,'Qm':0.28,'u':0.01,'d':0.0049}; pars.update(diction)
#diction = {'vI':0.023,'vQ':0.014,'Im':0.081,'Qm':0.05,'u':0.01,'d':0.0049}; pars.update(diction)

# Parameters for the feeding function

diction = {'begin':200.0, 'divisions':40.0,'FT':0.0}; pars.update(diction)

# Parameters for the Insulin Equation
diction = {'sigma':0.48,'rI':0.47,'epsilon':0.02}; pars.update(diction);  
 
# Parameters for the Glucose Equation
diction = {'a':17.97,'cL':0.66,'cM':0.34,'rG':0.2,'Ge':5.0,'v':18.69,'Gbeta':6.48,'alpha':2.72,'GL':1.4,'GM':4.0,'F':0.0}; pars.update(diction) 

# Parameters for Cortisol Function
#diction = {'AG':0.32,'Am':0.16,'del':780.0,'Tc':1440.0,'Tu':75.0,'B':0.04,'Q':0.0}; pars.update(diction);
diction = {'AG':0.32,'Am':0.16,'del':780.0,'Tc':1440.0,'Tu':75.0,'B':0.04,'Q':0.0}; pars.update(diction);

#  Parameters for Feeding and Insulin Inhibition Functions
diction = {'omegah':1.0, 'Dh':0.0,'omegaf':1.0, 'Df':0.0, 'kQ':0.4, 'n':4.48, 'A':3.7, 'tau':30.0,'starttime':-500.0, 'endtime':2880.0, 'step':1.0/100.0}; pars.update(diction);

# Parameters of state
diction = {'hQ':True,'Dex':0}; pars.update(diction)


# Data that is used for the variation

ChP='sigma'

Prs = np.round(pars[ChP]*np.arange(1,2.1,0.4),decimals=2)
FTs = np.arange(0,1440,75)
